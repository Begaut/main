import styled from 'styled-components';

import { bigLogo, PlayBtn, Edit as EditOrigin } from '@main/assets';
import { device } from '@main/__data__/constants';

const calcResponsive = (min, max) =>
  `calc(${min}px + (${max} - ${min}) * ((100vw - 768px) / (1440 - 768)))`;

export const Wrapper = styled.section`
  font-family: Montserrat;
  font-size: 12px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.17;
  letter-spacing: normal;
  margin-bottom: 104px;
  display: flex;
  align-self: center;
  align-items: center;
  justify-content: center;
  justify-self: center;
  max-height: 698px;
  background-size: ${calcResponsive(400, 892)} ${calcResponsive(200, 326)};
  background-repeat: no-repeat;
  padding: 0 20px;
  flex-direction: column-reverse;
  margin-top: 64px;

  @media ${device.tablet} {
    background-image: url(${bigLogo});
    margin-top: 0;
    flex-direction: row;
  }

  @media (min-width: 1440px) {
    padding: 0;
    background-size: 892px 326px;
  }
`;

export const VideoBlock = styled.div`
    margin-top: 32px;
    min-width: 272px;
    height: 194px;
    opacity: 1;
    border-radius: 16px;
    box-shadow: 0 16px 64px 0 rgba(24, 40, 151, 0.4);
    background-color: #003eff;
    max-width: 558px;
    max-height: 398px;
    display: grid;
    font-size: ${calcResponsive(80, 120)};

    @media ${device.tablet} {
        min-width: 38.75%;
        margin-top: 56px;
        height: ${calcResponsive(220, 398)};
        margin-right: ${calcResponsive(40, 123)};
    }

  svg {
    align-self: center;
    justify-self: center;
    width: 1em;
    height: 1em;
    transition: opacity 0.2s ease-in-out;
    opacity: 0.8;
  }

  svg:hover {
    opacity: 1;
  }
`;

export const StyledText1 = styled.div`
    font-size: 22px;
    margin-top: 15px;
    margin-bottom: ${calcResponsive(20, 32)};
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #1e1d20;

    @media ${device.tablet} {
        font-size: ${calcResponsive(32 * 0.45, 32)};
    }

    @media (min-width: 1440px) {
        font-size: 32px;
    }
`;

export const StyledText2 = styled.div`
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #1e1d20;

    @media ${device.tablet} {
        font-size: ${calcResponsive(24 * 0.45, 24)};
    }

    @media (min-width: 1440px) {
        font-size: 24px;
    }
`;

export const TextBlock = styled.div`
    align-items: center;

    @media ${device.tablet} {
        max-width: 32%;
    }
`;

export const Edit = styled(EditOrigin)`
    margin-left: 10px;
    cursor: pointer;
    width: 32px;
    height: 32px;
`;
